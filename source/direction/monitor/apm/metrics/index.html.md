---
layout: markdown_page
title: "Category Direction - Metrics"
---

- TOC
{:toc}

## Metrics

### Introduction and how you can help
Thanks for visiting this category strategy page on Metrics in GitLab. This category belongs to and is maintained by the [APM](/handbook/engineering/development/ops/monitor/APM/) group of the Monitor stage.

Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your Metrics usage, we'd especially love to hear your use case(s).

* [Maturity Plan](#maturity-plan)
* [Related Issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aapm&label_name[]=Category%3AMetrics)
* [Monitor Stage Direction Page](/direction/monitor/)

## What are metrics 

Metrics help users understand the health and status of your services and essential for ensuring the reliability and stability of those services., metrics normally represents by raw measurements of resource usage over time (e.g. measure memory usage every 10 second). Some metrics represent the status of an operating system (CPU, memory usage). Other types of data tied to the specific functionality of a component (requests per second, latency or error rates).
The most straightforward metrics, to begin with, are those already exposed by your operating system hence easier to collect (e.g. Kubernetes metrics).
For other components, especially your applications, you may have to add code or interfaces to expose the metrics you care about. Exposing metrics is sometimes known as [instrumentation](https://about.gitlab.com/direction/monitor/apm/workflow/instrument/), the collection of metrics from an end point is called scraping.

## Our mission

Provide users with information about the health and performance of their infrastructure, applications, and system for insights into reliability, stability, and performance.

## Target audience 

Metrics are essential for all users across the DevOps spectrum. From developers who should understand the performance impact of changes they are making, as well as operators responsible for keeping production services online.

Our vision is that in 1-2 years, GitLab metrics is the main day-to-day tool for monitoring cloud-native applications for SMBs. 

We are targeting [Software Developer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer) and [DevOps Engineer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer) working in a SMB from the following reasons: 

* SMBs are more likely to be cloud-native - no doubt that the trend for large enterprises is going cloud-native. However, they will remain hybrid and need a full-blown monitoring solution to cover both approaches. 
* Cloud-native is one of the [fastest-growing markets in IT](https://www.gartner.com/smarterwithgartner/cloud-shift-impacts-all-it-markets), given how GitLab is positioned in this space going after these segment will allow us to gain market share and quickly expand.
* Developers in this market segment are more likely to be a complete DevOps engineer, responsible for development, deployment and monitoring.
* SMBs are less willing to spend their budget on expensive monitoring systems - our base monitoring features are free for all GitLab users.
* This strategy means that it is less likely we'll compete head-on with the established vendors there are in the market today. 

## Current experience 

The experience today offers you to deploy Prometheus instance into a project cluster by a push of a button, the Prometheus instance will run as a GitLab managed application. Once deployed, it will automatically collect key metrics from the running application which are displayed on an out of the box [dashboard](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#using-the-metrics-dashboard). Our dashboards provide you with the needed [flexability](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#defining-custom-dashboards-per-project) to display any metric you desire you can set up alerts, configure [variables](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#templating-variables-for-metrics-dashboards) on a generic dashboard, drill into the [relavant logs](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#view-logs-ultimate) to troubleshoot your service and more... 
If you already have a running Prometheus deploy into your cluster simply [connect](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#external-prometheus-instances) it to your GitLab and start using our GitLab metrics dashboard.

## Targeted workflows

The target workflow, which is listed below, is our [high-level roadmap](https://app.mural.co/t/gitlab2474/m/gitlab2474/1589786898353/a20f7938166fd7bc444b8c9cb17f447fcbe5f577). It is based on competitive analysis, user research, and customer interviews—the details of each workflow listed in the epics and issues.

### Getting data in

The first step in application performance management is collecting the proper measurements. Getting accurate data is critical in order to know the health of your services and application. Our metric solution is powered by Prometheus. This means that Prometheus will collect infrastructure and application metrics, in order to achieve this, we'll need to make sure our users are successfully 
1. Installing Prometheus into a K8s cluster - Whether you have a preinstalled Prometheus instance on your cluster, or you'd like us to deploy Prometheus for you our metric solution should be agnostic to your preferred method
2. Deploy exporters and instrument app - We should help you as much as possible with instrumenting your applications and deploy exporters into a cluster.
3. Enable metrics scraping - We need to an easy way to enable metrics scraping. When possible, we should strive to do this automatically. 

#### Supported research and epic
- [Instrument Prometheus metrics](https://gitlab.com/gitlab-org/ux-research/-/issues/553)  
- [Instrument to viable](https://gitlab.com/groups/gitlab-org/-/epics/2316)


### See metric in a dashboard

Once you've collected a set of metrics, the next step is to see those metrics in a dashboard.

1. Out of the box dashboard (OOTB) - Everyone loves beautiful dashboards, however building one is a challenge; the more OOTB dashboard we can provide, the better. Established vendors take pride in the amount of OOTB dashboard they can provide to their customers. We could do them same and leverage GitLab community with a growing library of dashboards that anyone can contribute to.
2. Data visualization (Dogfooding) - Information and action on data can help  understand the root cause of the problem quickly and accurately to conduct fast and effective troubleshooting - annotations, templating, drilldowns and improved visuaization - this is where most of our dogfooding metrics effort is focused on today.
3. Add a metric and customize your dashboard - Adding a metric to a dashboard is a basic functionality a monitoring solution should have. This can be quite confusing for a first time GitLab Metrics user. We intend to make this step easy and discoverable for our users today. We do believe that by building an onboarding experience and providing an easy way to add any metric to a dashboard, we can increase user adoption.

#### Supported research and epic
- [What types of dashboards we should build for our users](https://gitlab.com/gitlab-org/ux-research/-/issues/533)
- [Understand how our users use Grafana](https://gitlab.com/gitlab-org/ux-research/-/issues/748)
- [Add metric to a Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2863)
- [Dogfooding metrics](https://gitlab.com/gitlab-org/monitor/apm/-/issues/28)

### Alerting
Users need to be alerted on any threshold violation, ideally before their end-users
1. Setting up an alert - Should be a straight forward action supported by all metrics and most of the chart types.

### Kubernetes monitoring
Our vision is that in 1-2 years, GitLab metrics will be the primary day-to-day tool for monitoring cloud-native applications. To achieve that we would need to support:
1. Cluster insight - Provide our users insight to cluster, pod and health.
2. Key metrics and OOTB dashboard - Curated and scalable dashboards that provide out of the box k8s metrics and bird's eye view across environments. 

#### Supported research and epic
- [Typical workloads and metrics to collect on Kubernetes deployments](https://gitlab.com/gitlab-org/ux-research/-/issues/389)
- [Infrastracture dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2135)

### Log aggregation

In the distributed nature of cloud-native applications, it is crucial and critical to collect logs across multiple services and infrastructure, present them in an aggregated view, so users could quickly search through a list of logs that originate from multiple pods and containers. You can review our logging direction page for more information on out logging maturity plan, but in a nutshell, we'd like to surface aggregated logs across multiple services and applications in a single UI in GitLab.

## What's Next & Why

### Dogfooding metrics
The APM team's current focus is on [dogfooding metrics](https://gitlab.com/gitlab-org/monitor/apm/-/issues/28). The Monitor APM team is closely working with the infrastructure team to migrate dashboards used for monitoring Gitlab.com from Grafana to GitLab Metrics. 
There has been significant progress to date (20 dashboards migrated) and working together has resulted in identifying [critical](https://gitlab.com/groups/gitlab-org/-/epics/2541) and [non-critical](https://gitlab.com/groups/gitlab-org/-/epics/2597) gaps in GitLab Metrics. 

This initiative will have reached a significant milestone once we [replace and turn off the SLA dashboard](https://gitlab.com/gitlab-com/runbooks/-/merge_requests/2233). 
Doing so will mean that we are using GitLab to monitor our SaaS business in a real and meaningful way.

After this important Milestone, the APM team will continue to partner with our internal teams and experts for collaboration and feedback.