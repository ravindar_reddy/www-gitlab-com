---
layout: handbook-page-toc
title: "Cadence Call Topic Suggestions"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

----
This page covers suggested topics to bring up during your cadence calls. These are simply suggestions for when you don't have enough items on your agenda. There are two sections, [General Suggestions](/handbook/customer-success/tam/cadence-calls/#general-suggestions) is for topics that are good at any time and [Ephemeral Suggestions](/handbook/customer-success/tam/cadence-calls/#ephemeral-suggestions) is for topics that are ephemeral, such as release-specific topics or requests from Product Managers.

### General Suggestions

These are suggestions that can be used at any time on customer calls.

- Are you running the most recent version / patch of gitlab?
- Have you recently tested restoring from a backup?
- Are signups enabled? Do you want them to be?
- Revew upcoming features / releases (specific questions and info can be found below)
- Stage adoption questions
- Discovery questions about their usage and processes
- Do your users have any feedback?
- Are there any areas for enablement or training?
- How are you leveraging other tools / integrations?
- What are your best practices and typical workflows?

### Ephemeral Suggestions

These are suggestions that can be used on customer calls, but are time-sensitive. Examples would be deprecated features, requests for information from Product, upcoming release highlights, etc.

- PostgreSQL update
- Only/Except deprecation
- Release 13.0 highlights
