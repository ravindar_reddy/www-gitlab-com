---
layout: handbook-page-toc
title: "Success Plans"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

----

- [Success Plan Highlights and Things to Keep in Mind](https://www.youtube.com/watch?v=qY--n6Obexk&list=PL05JrBw4t0KpThYLhg5uck84JLxljUi_f&index=3&t=0s)
- [Success Plan FAQs](https://www.youtube.com/watch?v=bfxy8mXnudE&list=PL05JrBw4t0KpThYLhg5uck84JLxljUi_f&index=4&t=0s)
- [Gainsight Update: Feb 2020](https://www.youtube.com/watch?v=mV-72Nga_z0&list=PL05JrBw4t0KpThYLhg5uck84JLxljUi_f&index=2&t=0s)

## What is a Success Plan?

There are two types of success plans in Gainsight. The first is the ROI Success Plan. This success plan is a customer facing interactive map to align the purchase (or renewal!) reasons to customer outcomes, develop joint accountabilities, measure progress and evolve as customers’ needs change. Put another way, it’s the way the customer can see and know that they’re attaining significantly more value than the cost of a product or service.

The second type is the Stage Adoption Success Plan. This success plan is an internal document allows the TAM to track progress and goals on driving stage adoption within an account.

Slide deck: [Success Plans: Foundation for Success](https://docs.google.com/presentation/d/15Qt-UcfRt9cX-4CV7zMsurojTZg_8Kf-u0dMYL16JXQ/edit?ts=5e398d2d#slide=id.g6e9082c768_3_234)

## What is NOT a Success Plan?

- Collaboration projects are tactical, and less strategic
- Collaboration projects may show some element of strategy and progress, however it is harder to measure and consume that information in other systems
- GitLab issues are difficult to measure, especially across projects and groups
- A key element of TRANSPARENCY is clarity and focus. If a strategic document becomes noisy with day to day activities, it becomes less transparent.

## What’s the purpose of a Success Plan?

- Document the customer’s desired outcomes (e.g., KPIs, problems to solve, capabilities to provide, benefits to achieve)
- Align GitLab’s product adoption plan to customer outcomes
- Define a activities (e.g., change management, training) roadmap and timeframes for a successful adoption journey
- Develop shared understanding and commitment to the plan between GitLab and customer stakeholders
- Serve as baseline to track delivery of outcomes, adoption and issues / requests
- Provide foundation for supporting processes like health checks and business reviews

### What are the strategies and tactics of a Success Plan?

- Who will use the product or service?
- Which product features or areas of service will be used and when?

### How will a Success Plan be used?

A Success Plan is the guiding document which connects the customer’s pain (often purchasing reason) to their GitLab solution to verified business outcomes. EBRs will  showcase the status of a Success Plan.

## How to Create a Customer Facing ROI Success Plan in Gainsight

### Summary

To create a success plan, the success plan author will perform the following activities:

1. Understand the customer’s motivation for purchasing the GitLab product through reviews with the sales team
1. Identify the customer’s business objectives.  
1. Socialize and confirm the business objectives with the customer.
1. Document the success plan in the Gainsight customer success tool
1. Track realization of the objectives outlined in the success plan.


### Step 1: Understand the Customer’s Motivation

#### New Customer Intake

- Review the opportunity Command Plan
- Collaborate with SAL/SA to confirm:
  - Strategy and Value Driver (Increase Operational Efficiencies, Deliver Better Products Faster, and/ or Reduce Security and Compliance Risk)
  - Highlights
  - Customer Positive Business Outcomes (PBOs)
  - Stakeholders
  - Potential risks, issues and barriers
  - Customer information (i.e., environment, tools, communication preferences, etc.)

#### Existing Customer

- You likely already know much about the customer and their needs...
- Collaborate with SAL+SA regarding their perceptions of the customer’s needs and what new desired outcomes they have
- Review the Command Plan, if applicable

### Step 2: Identify Business Objectives

#### What are the objectives of a Success Plan?

The objectives should be written from the customer’s perspective and in their words, not ours. For example, an Objective could be “Reduce our Cycle Time from X to Y - from Issue creation to production” rather than “find value from utilizing Manage stage” or “get customer to adopt X stage”.

You should be able to answer:

- How will the customer  track the benefits of purchasing your product or service?
- How will the customer measure success?
- How is the customer currently measuring and do they have those metrics?

Process:

- Before the “Welcome Kick Off Call, set expectations that this conversation needs to occur, and describe whose perspectives are needed
- Drive this conversation during the “Welcome” call
  - May need to be more than one call
- Consider all stakeholders who might have important input, both laterally and vertically within the customer organization
- Review Command Plan (CP) and **validate** perceptions of customer needs you learned from Sales:
  - What is the _Primary Value Driver_? (CP)
  - What is their _Primary Use Case_? (CP)
  - _Why GitLab_? (CP)
  - What are their _Metrics_? (CP)
- Work to understand the objective, ask follow-up questions, even if you think you know the answers. Be sure that the questions are open-ended. For example:
  - Why is this important?
  - Who will notice or benefit from this outcome?
  - Who will contribute to the effort?
  - When do we need to reach this goal?
- What _verifiable_ outcomes or ROI is the customer pursuing?

<table>
  <tr>
   <td> <strong>Type</strong> </td>
   <td> <strong>Numerical Goals - Quantitative</strong> </td>
   <td> <strong>Initiatives - Qualitative</strong> </td>
  </tr>
  <tr>
   <td><strong>Description</strong></td>
   <td>A lagging indicator such as time saved or money earned</td>
   <td>Description of an outcome that will be a substantial strategic win for the Org.</td>
  </tr>
  <tr>
   <td><strong>Example</strong></td>
   <td>
     <ul>
      <li>20% reduction in Cycle Time</li>
      <li>XX% change to Deployment Frequency</li>
     </ul>
   </td>
   <td>
    <ul>
     <li>“Migrate to single App for DevOps lifecycle”</li>
     <li>“Launch SAST and DAST across the company”</li>
    </ul>
   </td>
   </tr>
    <tr>
   <td><strong>Common Pitfalls</strong></td>
   <td>
    <ul>
    <li><span style="text-decoration:underline;">Unrealistic</span>: we can easily say “50 increase in IACV”</li>
    <li><span style="text-decoration:underline;">Not the <em>real</em> goal</span>: “activate 50% of users” when we really want to measure the <em>impact</em> of GitLab to their bottom line</li>
    <li><span style="text-decoration:underline;">Not measurable</span>: “Delight the customer!”</li>
    </ul>
   </td>
   <td>
    <ul>
    <li><span style="text-decoration:underline;">In the weeds</span>: technical jargon that your contact may not, but not the decision-maker, sponsor, or Exec team</li>
    <li><span style="text-decoration:underline;">Nice-to-have or Out-of-date</span>: are we focused on the #1 or #2 company initiative, or #5 or #6?</li>
    <li><span style="text-decoration:underline;">Ambiguous</span>: “more things faster” or “increase team collaboration”</li>
    </ul>
   </td>
    </tr>
</table>

### Step 3: Socialize and Confirm the Business Objectives

#### Purpose

All stakeholders agree on the business objectives that the project is pursuing, which products & services will help them reach that goal, and to keep the scope focused to 1-3 objectives. As you close out objectives, you can add new ones.

#### Key Topics

- Which of your products and/or services are required to support the business objective?
  - Start by validating what we understand to be the customer’s pain and your best practices for the group to adapt from there
- For each business objective:
  - What is its priority relative to the others? Rank them
  - Which stakeholders will benefit?
  - How should we prioritize efforts for your teams?
  - Are there obvious quick wins? (recommend then adapt)
  - Deadlines for various steps?
- Optional: Tasks within each business objective
  - Key contributions & responsibilities of your team
  - Key contributions & responsibilities of the customer’s team

### Step 4: Documenting the Success Plan

#### Create a success plan object in Gainsight

Gainsight is the Customer Success tool employed by GitLab to track customer success plans.  To create a success plan, perform the following steps:

1. From the Gainsight NXT Homescreen, navigate to the customer for whom you want to build a success plan
1. From the menu on the left-hand side of the screen, choose "Success Plan"
1. In the upper right-hand side, click "+ Success Plan"
1. Add a Name for the Success Plan, usually "Customer Name Success Plan" for customer facing plans or "Customer Name Stage Adoption Success Plan" for internally facing plans
1. Add Type as "ROI Success Plan" for customer facing plans or "Stage Adoption" for internally facing success plans
1. Set the "Due Date" as end of GitLab's fiscal calendar or a date logical given the content of the success plan

#### Add Objectives to the Success Plan

Customers can have multiple Objectives, though it’s best to limit it to 1-3 for focus and achievability.

Key components of the objectives  in the Success Plan include:

- **Name**: Record in the Objective title, such as “Cut Time to First Commit by 20%”
- **Owner**: Record who is invested in this objective
- **Due Date**: Record the due date for the objective
- **Category**:  Indicate if the objective is Stage Adoption, Rev Expansion, or ROI Success
- **Status**: Mark the objective “New” or “Work in Progress”
- **Priority**: Indicate if the priority is Low, Medium, or High
- **Success Criteria**: When you first create the Objective (and each time you check in with the customer), log an Activity update on it to capture the progress
- **Comments**: Share any info that might be relevant to the objective (e.g. potential blockers, architecture details, or other important details worth mentioning)

##### Adding tasks to an objective

Objectives should be actionable, and Gainsight provides a way to create action items as part of the objective, which are listed as tasks. A **task** in Gainsight is equivalent to a **milestone** in GitLab's success plan terminology.

To create a task, perform the following steps:

1. Click on the objective so that the side panel appears on the right.
2. To the right of the objective due date, click the icon of the three dots in a vertical line, and click "Add Task".
3. Fill out the details of the task:
   - **Name**: The title of the task, such as "Establish baseline metrics for comparision".
   - **Owner**: The person who is responsible for this task.
   - **Due Date**: When the task should be completed by.
   - **Status**: Is the task ongoing, or completed?
   - **Priority**: In relation to other tasks for the objective, how important is it?
   - **Description**: Any additional details that are helpful to understanding what the task is. This could include external references, risk factors that could impact the task, etc.
   - **Milestone Details**: Additional details specific to our success plan structure.
     - **Customer DRI**: Who on the customer side is responsible for the task?
     - **Milestone Risk**: How significant are the risk factors that could impact being able to complete the task?
     - **Progress (%)**: How far along is the task?

Tasks will affect the overall completion of the objective, and provide more granular visibility into progress on the objective when looking at the [Gantt chart](#review-resulting-gantt-chart).

#### Review Resulting Gantt Chart

Navigate to the Gantt chart tab in the success plan and confirm the representation captures the plan.  Adjust dates accordingly.

### Step 5: Track Value

1. Drive Progress
2. Record and Demonstrate Success
3. Validate and Update Business Objectives

#### Drive Progress

- Share with Sales Team to keep them in the loop with customer
- Share Success Plans to show next steps to customer, and to review progress internally
- Use EBRs (and/or other recurring meetings) to review steps achieved and set next steps
- Health check is a tactical measure of product adoption, barriers / issues (i.e., support, product requests) and sentiment as a measure of adoption progression

#### Record and Demonstrate Success

- During EBRs and regular meetings, log an Activity on the Objective to record how the customer is trending towards their goals
- Also post other progress updates and call notes on SFDC
- Show progress towards goals
  - Show to customer with the Success Plan
  - Share internally with quarter-over-quarter reports (trends and objectives achieved)
  - Use progress to drive the ROI scorecard measure in Outcomes Health
- Before closing an Objective, get confirmation from a customer (ideally in writing) that it has been achieved. Record it as an Activity on the Salesforce record, and mark the Objective as closed.

#### Validate and Update Business Objectives

- As a customer’s business and strategies change, so will the value that they need from you
- Show the Success Plan to the customer regularly to help keep a fresh understanding of their needs
  - Advanced: email a tabular report to the customer once a month, listing the objectives and inviting them to reply if they’re out of date
- Identify the key times when you interact with a customer, when a discussion of business goals feels appropriate and the right stakeholders are at the table
  - Key handoffs between teams
  - EBRs
  - Exec check-ins
- Use a custom date field on the Objective in the Success Plan, “Last Validated”. CSM updates it when they get confirmation from the customer that it is still a business priority for them.
