---
layout: handbook-page-toc
title: Training
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the Training page! This page is our [SSOT page](https://about.gitlab.com/handbook/documentation/) for all training related items at GitLab. Training has been organized by functional groups and been linked to their location in the handbook. 

GitLab encourages functional groups to develop functional specific training for personal growth and professional development. When creating training, please notify the Learning & Development Team at `learning@gitlab.com` and submit an MR to update the Training page with the link and title of the topic. This page serves as the central locaitons for all trainings at GitLab.

### GitLab General 

*  [GitLab Training tracks](/training/)
*  [GitLab University](https://docs.gitlab.com/ee/university/)
*  [Learn @ GitLab](https://about.gitlab.com/learn/)
*  [Pathfactory flows - example path](https://learn.gitlab.com/c/a-beginner-s-guide-t?x=GVkN_U&lb_referrer=https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)

### Legal

*  N/A

### Finance

*  N/A

### People

*  Learning Sessions
   *  [Live Learning](/handbook/people-group/learning-and-development/#live-learning) Sessions 
      *  [Ally Training](/company/culture/inclusion/ally-training/)
      *  [Inclusion Training](/company/culture/inclusion/inclusion-training/)
      *  [Receiving Feedback](/handbook/people-group/guidance-on-feedback/#responding-to-feedback)
      *  [Communicating Effectively & Responsibly Through Text](/company/culture/all-remote/effective-communication/)
      *  [Compensation Review: Manager Cycle (Compaas)](https://www.youtube.com/watch?v=crkPeOjkqTQ&feature=youtu.be) - just a youtube video
   * [Action Learning](/handbook/people-group/learning-and-development/#action-learning) 
   * [Leadership Forum](/handbook/people-group/learning-and-development/leadership-forum/)  
*  [Certifications & Knowledge Assessments](/handbook/people-group/learning-and-development/certifications/)
   *  [Values](/handbook/values/#gitlab-values-certification)
   *  [Communication](/handbook/communication/#communication-certification)
   *  [Ally](/company/culture/inclusion/ally-training/#ally-certification)
   *  [Inclusion](/company/culture/inclusion/inclusion-training/#inclusion-certification)
   *  [GitLab 101](/handbook/people-group/learning-and-development/certifications/gitlab-101/)
*  [Career Development](/handbook/people-group/learning-and-development/career-development/)
*  [Anti-Harassment](/handbook/people-group/learning-and-development/#common-ground-harassment-prevention-training) 
*  [New Manager Enablement](/handbook/people-group/learning-and-development/manager-development/)
* [People Specialist team and People Experience team training issue](https://gitlab.com/gitlab-com/people-group/people-operations-and-experience-team-training/-/blob/master/.gitlab/issue_templates/new-hire-training.md)
* [People Specialist team - Contractor Conversion Starter Kit](https://gitlab.com/gitlab-com/people-group/people-operations-and-experience-team-training/-/blob/master/.gitlab/issue_templates/new-hire-training.md)
* [Onboarding](/handbook/general-onboarding/)/Training Issues
   *  [Onboarding Issue](https://gitlab.com/gitlab-com/people-group/employment-templates-2/-/blob/master/.gitlab/issue_templates/onboarding.md)
   *  [Interview Training](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/interview_training.md)
   *  [Becoming a GitLab Manager](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md)
* [Leadership Toolkit](/handbook/people-group/leadership-toolkit/)

### Engineering

*  [Developer Onboarding](/handbook/developer-onboarding/) 
*  [Engineering Quality Team Onboarding](/handbook/engineering/quality/onboarding/ )
*  [Support Onboarding](/handbook/support/onboarding/)
*  [Rails Performance Workshop](https://drive.google.com/drive/folders/1kXMilTokysTZ0GtnRrS3Ru132AYe0-_g )
*  [Calendar Blocking Workshop](https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/441)
*  [UX Research Training](/handbook/engineering/ux/ux-research-training/)
*  [UX Designers Guide to Contributing to UI Changes](/handbook/engineering/ux/ux-resources/designers-guide-to-contributing-ui-changes-in-gitlab/)

### Product

*  [Product Onboarding](https://gitlab.com/gitlab-com/Product/-/blob/master/.gitlab/issue_templates/PM-onboarding.md)
   *  [Iteration Training](https://gitlab.com/gitlab-com/Product/-/blob/master/.gitlab/issue_templates/iteration-training.md)

### Marketing

*  [Marketing Onboarding](https://gitlab.com/gitlab-com/marketing/onboarding/blob/master/.gitlab/issue_templates/non_xdr_function_onboarding.md)

### Sales

*  [Sales Training Handbook](/handbook/sales/training/)
*  [Sales Quick Start](/handbook/sales/onboarding/sales-learning-path/)
*  [Sales Enablement Sessions](/handbook/sales/training/sales-enablement-sessions/)
*  [Sales Kick Off (SKO)](/handbook/sales/training/SKO/)
*  [Customer Success Skills Exchange](/handbook/sales/training/customer-success-skills-exchange/)
*  [Customer Success GitLab Demos platform & catalog](https://gitlabdemo.com/)

### Channel

*  [Reseller onboarding](/handbook/resellers/onboarding/)

### Professional Services

*  [Framework](/handbook/customer-success/professional-services-engineering/framework/)
*  [SKUs](/handbook/customer-success/professional-services-engineering/SKUs/)
*  [Customer Services Guided Explorations](https://gitlab.com/guided-explorations)

