---
layout: handbook-page-toc
title: "Preferred Companies"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

We use this preferred companies list for two reasons; it's used internally by our sourcing team, plus we hope that if you work, or have worked, at one of the following companies you feel encouraged to apply. Of course, this list is not definitive and it is not necessary to have this experience to work at GitLab, so we encourage everyone else to apply as well.

We try to avoid mentioning [partners](/partners/) on this page because it might hurt the relationship.

## DevOps Stages

GitLab is a single application for the complete DevOps Lifecycle. As such, we're highly interested in hiring individuals with experience from companies who create products related to specific parts of this lifecycle.

### Common

* AirBnB
* Alcide
* Amazon
* Capital One
* Chef
* Datera
* Dell
* DigitalOcean
* Docker
* f5
* Facebook
* Google
* Heroku
* Joyent
* Lyft
* Mesosphere
* Microsoft
* Netflix
* New Relic
* NPM
* Oracle
* Palo Alto Networks
* Pivotal
* Puppet
* Qualcomm
* Rackspace
* RedHat
* Shopify
* Sonarcube
* Stripe
* Sumologic
* Twilio
* Twitch
* Uber
* VMware
* Workday
* WP Engine

### Manage

#### Access

* Auth0
* Azure AD
* Broadcom
* Evidian
* ForgeRock
* IBM
* Idaptive
* Micro Focus
* Okta
* OneLogin
* Optimal IdM
* Oracle
* Ping Identity
* SailPoint
* SecureAuth
* Symantec

#### Compliance

* AuditBoard
* Cobalt
* Collabnet
* Galvanize
* LogicGate
* Micro Focus
* Reciprocitylabs
* ServiceNow
* ZenGRC

#### Analytics

* Code Climate
* Elastic
* Micro Focus
* Pluralsight
* Plutora
* Splunk
* Tasktop 
* XebiaLabs

#### Import

* AgileCraft
* Aha!
* Asana
* CollabNet
* GitHub
* Jenkins
* Rally
* Tasktop
* Trello
* VersionOne
* XebiaLabs

#### Spaces

* Asana
* GitHub
* JetBrains
* ServiceNow


### Plan

#### Project 

* Asana
* Basecamp
* Clarizen
* LiquidPlanner
* Mavenlink
* Monday.com
* Microfocus
* Planview
* Podio
* Quickbase
* Quip
* Sciforma
* Smartsheet
* Sopheon
* Targetprocess
* Trello
* Upland
* Workfront
* Wrike

#### Portfolio

* AHA.io
* Changeview
* Microfocus
* Planview
* Planisware
* Productboard
* Rally
* Roadmunk
* Sciforma
* Trello


#### Certify

* Jama Connect
* IBM Rational DOORS
* qTest
* Test Rail
* HPQC
* Zendesk
* Freshdesk

#### Certify

* Zendesk
* HubSpot
* Intercom

### Create

#### Source Code

* Azure
* Gerrit
* GitHub
* Perforce
* Phacility

#### Knowledge

* Abstract
* Adobe
* Avocode
* DropBox
* Figma
* Framer
* GitHub
* Google Docs
* Invision Studio
* Notion
* Roam Research
* Storybook UI
* UX Pin
* Webflow
* Zeplin

#### Static Site Editor

* Contentful
* Forestry.io
* Gatsby
* Gridsome
* Jekyll+
* Kontent.ai
* Netlify CMS
* Nuxt
* Prose
* Siteleaf
* Sourcebit
* Squarespace
* Stackbit
* TinaCMS
* Toast UI
* Webflow

#### Editor

* Blocks
* Cloud9
* Coder
* Codesandbox
* Gist.io
* GitHub
* Gitpod
* Koding
* Repl.it
* StackBlitz
* Theia
* Visual Studio One

#### Gitaly

* GitHub
* Perforce

#### Gitter

* Circle
* Comradery
* Discord
* Freenode

#### Ecosystem

* GitHub
* HashiCorp
* RedHat
* Salesforce
* Stripe
* Twilio

### Verify

* Cloudbees
* CircleCI
* Travis CI

### Release

* Codeship
* Netlify
* Zeit
* Plutora

### Package

* codefresh
* goHarbour
* JFrog
* NPM
* Quay
* Sonatype

### Configure

* Serverless
* Cloudreach

### Monitor

* Datadog
* Dynatrace
* Elastic
* Influxdata
* Lightstep
* Netsil
* New Relic
* SignalFx
* Splunk/VictorOps
* Pluralsight
* Tasktop 

### Secure

* Anchore
* Appscan
* Blackduck
* Checkmarx
* Coverity
* Kata Containers
* Micro Focus
* Microsoft
* Snyk
* Sonarqube
* Synopsys
* Twistlock
* Veracode
* Whitesource


### Defend

#### Insider Threat

* [A10Networks](https://www.a10networks.com/)
* [Absolute](https://www.absolute.com/)
* [AnhLab](https://global.ahnlab.com/)
* [Akamai](https://www.akamai.com/)
* [Alterlogic](https://www.alertlogic.com/solutions/threat-detection-response)
* [AT&T Cybersecurity/Alienvault](https://cybersecurity.att.com/)
* [Anomali](https://www.anomali.com/)
* [Anity](https://www.antiy.net/)
* [Armor](https://www.armor.com/)
* [Arxan](https://www.arxan.com/)
* [Avira](https://www.avira.com/)
* [Awake](https://awakesecurity.com/product/)
* [Azure Security Centre](https://docs.microsoft.com/en-us/azure/security-center/security-center-intro)
* [Bitdefender](https://www.bitdefender.com/)
* [Bitglass](https://www.bitglass.com/)
* [Bitsight](https://www.bitsight.com/)
* [Blueliv](https://www.blueliv.com/)
* [Braintrace](https://braintrace.com/)
* [Bwise](https://www.bwise.com/solutions/cyber-security/information-security-management)
* [Carbon Black](https://www.carbonblack.com/products/vmware-carbon-black-cloud/)
* [Cato](https://www.catonetworks.com/)
* [Centrify](https://www.centrify.com/privileged-access-management/privilege-threat-analytics/)
* [Check Point Threat Intelligence](https://www.checkpoint.com/products-solutions/threat-intelligence/)
* [Cipher](https://cipher.com/mdr-managed-detection-and-response/)
* [Cisco Stealthwatch](https://www.cisco.com/c/en/us/products/security/stealthwatch-cloud/index.html)
* [Claroty](https://www.claroty.com/)
* [Clearswift](https://www.clearswift.com/solutions/web-security)
* [Code42](https://www.code42.com/)
* [Comodo](https://www.comodo.com/)
* [Crowdstrike](https://www.crowdstrike.com/)
* [Cyberinc](https://cyberinc.com/isla/)
* [Cyberreason](https://www.cybereason.com/deep-threat-intelligence)
* [Cylance](https://www.cylance.com/)
* [Cyren](https://www.cyren.com/products/threat-indepth)
* [Darktrace](https://www.darktrace.com/en/)
* [Digital Defense](https://www.digitaldefense.com/solution-overview/)
* [Digital Guardian](https://digitalguardian.com/)
* [ePLus](https://www.eplus.com/solutions/security/security-outcomes/secure-mission-critical-data)
* [ESET](https://www.eset.com/int/business/services/threat-intelligence/)
* [Exabeam](https://www.exabeam.com/)
* [F5](https://www.f5.com/solutions/application-security/)
* [FireEye](https://www.fireeye.com/)
* [Forcepoint](https://www.forcepoint.com/)
* [GoSecure](https://www.gosecure.net/)
* [HP Sure Click](https://www8.hp.com/us/en/solutions/sure-click-enterprise.html)
* [Imperva](https://www.imperva.com/) 
* [Logrythm](https://logrhythm.com/)  
* [McAfee](https://www.mcafee.com/enterprise/en-gb/home.html)
* [NetScout](https://www.netscout.com/arbor-ddos)
* [Palo Alto Networks](https://www.paloaltonetworks.com/)
* [Rapid7](https://www.rapid7.com)
* [Sonotype](https://www.sonatype.com/nexus-intelligence)
* [Splunk](https://www.splunk.com/) 
* [Symantec](https://securitycloud.symantec.com/cc/#/landing) 
*


#### Container Security

* [Akamai](https://www.akamai.com/us/en/resources/application-firewall.jsp)
* [Alertlogic](https://www.alertlogic.com/solutions/web-application-firewall/)
* [Alibaba Cloud WAF](https://www.alibabacloud.com/product/waf)
* [Anchor Enterprise](https://anchore.com/enterprise/)
* [Aqua](https://www.aquasec.com/products/aqua-cloud-native-security-platform/)
* [Arxan](https://www.arxan.com/)
* [Attivo](https://attivonetworks.com/product/deception-technology/)
* [Axway](https://marketplace.axway.com/apps/187256/web-access-firewall---xss-functional-example#!overview)
* [Barracuda Networks](https://www.barracuda.com/products/webapplicationfirewall)
* [Capsulate Protect](https://capsule8.com/resource/capsule8-protect-product-overview/)
* [Check Point Container Security](https://www.checkpoint.com/solutions/container-security/)
* [Citrix Web App Firewall](https://about.gitlab.com/handbook/product/categories/#defend-stage)
* [Cloudflare](https://www.cloudflare.com/waf/)
* [CloudPassage Container Secure](https://pages.cloudpassage.com/rs/857-FXQ-213/images/Container-Secure-data-sheet_03082018-1.pdf?_ga=2.80309470.834074949.1589860053-306212359.1589860053)
* [Comodo](https://www.comodo.com/)
* [Corero](https://www.corero.com/)
* [CyberArk](https://www.cyberark.com/)
* [Deepfence](https://deepfence.io/kubernetes-security/)
* [Exabeam](https://www.exabeam.com/)
* [Fortinet](https://www.fortinet.com/products/web-application-firewall/fortiweb.html)
* [Microsoft Azure WAF](https://docs.microsoft.com/en-us/azure/application-gateway/waf-overview)
* [NeuVector](https://neuvector.com/)
* [Oracle WAF](https://www.oracle.com/cloud/security/cloud-services/web-application-firewall.html)
* [Polyverse](https://polyverse.com/products/polymorphing-linux/)
* [Radware](https://www.radware.com/products/appwall/)
* [Signal Sciences](https://www.signalsciences.com/)
* [Stack Rox](https://www.stackrox.com/)
* [Twistlock](https://www.twistlock.com/)


### Enablement

* Look under [common](#common)

### Growth

Companies with a strong reputation for running effective Growth teams and experiments.

* AirBnb
* Facebook
* HubSpot
* Netflix
* Slack
* Spotify
* Twitter
* Uber

## Company Experience Based on Functional Area

Some companies demonstrate excellence in specific function. So we value experience from there regardless of how similar their product is to ours.

### Engineering Division

#### Development Department

Companies with strong reputation in product development reputation and/or SaaS solutions.

* Look under [DevOps Stages](#devops-stages)
* Adobe
* Salesforce

#### Infrastructure Department

* Facebook
* Google
* Amazon
* Twitter
* Spotify

#### Quality Department

Companies with a strong reputation in test engineering, and engineering productivity.

* Walmart Labs
* BlackBoard
* Expedia
* Houzz
* Credit Karma
* GitHub
* Circle CI
* Victor Ops
* SauceLabs
* Splunk
* Puppet
* Slack
* SalesForce
* Rackspace
* Workday
* Amazon
* Google
* Facebook
* Microsoft
* NetFlix
* VMware
* ThoughtWorks

#### Security Department

We strive to hire globally at GitLab, and the Security Department is very much focused on hiring globally. Our Security top-level objectives span an extremely broad expanse of domain knowledge. We've found that hiring from companies that have established security teams works best for bringing on the best people. The companies most likely to have established security teams are Global 2000 companies. 

* [Global 2000 Companies](https://www.forbes.com/global2000/)

#### Customer Support Department

* Rackspace
* GoodData
* Circle CI
* New Relic
* Zendesk
* Digital Ocean
* DreamHost
* NexGen Technologies
* Optiva
* WP Engine

#### UX Department

* AppDynamics
* Chartbeat
* Chartio
* DigitalOcean
* Fivetran
* HubSpot
* IBM
* Looker
* Palintir
* Sumo Logic
* Segment
* Sentry.io
* Stack Overflow
* Wavefront by VMware
* WeWork
